<?php
	if (isset($_FILES['gambar'])) {
		$gambar = $_FILES['gambar']["tmp_name"];
		$data = base64_encode(file_get_contents($gambar));
		$type = $_FILES["gambar"]["type"];
	}else{
		$data = "";
		$type = "";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>	</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
	<div class="col-md-12">
		<form method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="gambar" >Gambar</label>
				<input type="file" id="gambar" class="form-control" name="gambar">
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-default">
			</div>
		</form>
	</div>
	<div class="col-md-12">
		<textarea class="form-control" rows="10"><?=$data?></textarea>
	</div>
	<div class="col-md-12">
		<?php if (isset($_FILES['gambar'])) { ?>
		<img src="data:<?=$type?>;base64,<?=$data?>">
		<?php } ?>
	</div>
</div>
</body>
</html>